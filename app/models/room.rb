require 'singleton'

class Room
  include Singleton

  def initialize
    @players = []
  end

  def add_player(player)
    @players.push player
  end

  def remove_player(player)
    @players.delete player
  end

  # def find(id)
  #   @players.each do |p|
  #     return p if p == id
  #   end
  #   nil
  # end

  def players(except = nil)
    # $redis.set('chunky', 'bacon')

    @players
  end
end