class Notifier

  def initialize(game)
    @game = game
  end

  def update(message)
    broadcast_params = {}
    if message.is_a? ::Manchkin::Message
      broadcast_params = { type: :message, text: message.text }
    end

    if message.is_a? ::Manchkin::Action
      broadcast_params = { type: :action, action: message.action }.merge message.params.to_hash
    end

    # puts message.text
    # puts @game.current_player.id
    if message.type == :private
      ActionCable.server.broadcast "user#{message.player_id}", broadcast_params
    else
      @game.players.each do |player|
        ActionCable.server.broadcast "user#{player.id}", broadcast_params
      end
    end
    # NotifyJob.perform_later message.id.to_s, @game
  end

  # def pending_messages
  #   @messages.select { |message| !message.sent }
  # end
  #
  # def sent_messages
  #   @messages.select { |message| message.sent }
  # end



  # class Message
  #   attr_reader :id, :text, :type, :player_id
  #   # , :sent
  #
  #   def initialize(text, player_id = nil)
  #     @id = object_id
  #     @text = text
  #     @type = player_id.nil? ? :broadcast : :private
  #     @player_id = player_id
  #     # @sent = false
  #   end
  #
  #   def send!
  #     @sent = true
  #   end
  # end

end

