# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class RoomChannel < ApplicationCable::Channel

  # @@game = Room.instance

  def subscribed
    # p $redis.get('chunky')

    # GameRoom
    stream_from "room"
    $redis_onlines.set( params[:user_email], nil, ex: 10*60 )
    ActionCable.server.broadcast 'room', message: 'new user', user: params[:user_email]

    # pl = Player.new(User.find_by(email: params[:user_email]), params[:user_email])
    # p session
    # @@game.add_player pl
    # p GameRoom.players
    # p params
    # p GameRoom
    # @@players.push params[:user]
  end

  def unsubscribed
    p 'unsubscribed'
    $redis_onlines.del params[:user_email]
    ActionCable.server.broadcast 'room', message: 'user left', user: params[:user_email]

    # current_user = nil
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    # Message.create! content: data['message']
    ActionCable.server.broadcast 'room', message: 'new user'
  end

  # def away
  #   current_user.away
  #   p 'away'
  # end
end