module Api
  module V1
    class PlayersController < ApplicationController

      @@game = nil
      @@owner = nil

      before_action :new_game

      def action
        p params
        @@owner.open_door
        head :ok
      end

      # before_filter :authenticate_user!

      # respond_to :json

      def deck
        render :json => { deck: (@@game.cards).sort_by { |card| card.object_id } }
      end

      def in_room
        # p $redis.get('chunky')
        render :json => {players: $redis_onlines.keys}
      end

      def other_players
        # render :json => {players: @@game.players.find_all { |pl| pl != @@owner }.to_json }
        # render :json => {players: @@game.players.find_all { |pl| pl != @@owner }.map {::ObjectConverter.new(@@owner).other}.to_json }
        render :json => {
                      players: @@game.players.find_all { |pl| pl != @@owner}.map { |i| ::ObjectConverter.new(i).other }.to_json
                   }

      end

      def owner
        # p ::ObjectConverter.new(@@owner).owner

        render :json => {owner: ::ObjectConverter.new(@@owner).owner}
        # render :json => {owner: @@owner.to_json}

        # render :json => {owner: {id: 'wowow', name: 'leo', level: 1, power: 1, cards: [{id: '23', type: 'race', name: 'Эльф', first_ability: '+1 к Смывке', second_ability: 'Поднимаешься на 1 Уровень за каждого монстра которого помогаешь убить'},
        #                                                                                {id: '23q', type: 'race', name: 'Хафлинг', first_ability: 'что то про 1 абилку', second_ability: 'чтото про 2 абилку'}]}}

      end

      def index
        # sleep 3
        render :json => {owner: {}}
        # render :json => {owner: {id: 'wowow', name: 'leo', level: 1, power: 1, cards: [{id: '23', type: 'race', name: 'Эльф', first_ability: '+1 к Смывке', second_ability: 'Поднимаешься на 1 Уровень за каждого монстра которого помогаешь убить'},
        #                                                                                {id: '23q', type: 'race', name: 'Хафлинг', first_ability: 'что то про 1 абилку', second_ability: 'чтото про 2 абилку'}]}}
      end

      def game
        render :json => { game: {state: @@game.state} }
      end

      def battle
        render :json => { battle: ::ObjectConverter.new(@@game.battle).battle}
      end

      def new_game
        if @@game.nil?
          @@owner = ::Manchkin::Player.new 'leo'
          # p @@owner.to_json
          @@other1 = ::Manchkin::Player.new 'wowow'
          @@other2 = ::Manchkin::Player.new 'ololo'
          @@other3 = ::Manchkin::Player.new 'щзщзщз'

          @@logger = ::Manchkin::Logger.new


          @@game = ::Manchkin.start_game [@@owner, @@other1, @@other2, @@other3], @@logger

          notif = Notifier.new @@game

          @@logger.add_observer notif

        end
      end

    end
  end
end