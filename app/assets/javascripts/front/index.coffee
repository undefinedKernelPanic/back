@app = angular.module('Manchkin', [ 'ngRoute', 'Devise', 'LocalStorageModule'])
#var routeSegmentProvider = null;
#function Game($scope, e) {
#    console.log(e);
#}
app.config [
  '$routeProvider'
  ($routeProvider) ->
    $routeProvider
    .when('/',
      controller: 'login'
      templateUrl: 'login.html')

    .when('/login',
      controller: 'login'
      templateUrl: 'login.html')

    .when('/waiting_room',
      controller: 'waitingRoom'
      templateUrl: 'waitingRoom.html'
      resolve:
        onlinePlayers: [
          '$http'
          ($http) ->
            $http.get('/api/v1/online_players').then (w) ->
#              console.log w.data
              w.data.players
        ]
    )

    .when '/game',
      controller: 'gameCtrl'
      templateUrl: 'gg.html'
      resolve:
        owner: [
          'api'
          (api) ->
            api.getOwner()
        ]
        players: [
          'api'
          (api) ->
            api.getOtherPlayers()
        ]
        deck: [
          'api'
          (api) ->
            api.getDeck()
        ]
    return
]

app.config (localStorageServiceProvider) ->
  localStorageServiceProvider.setStorageType 'sessionStorage'
#app.run(['$http', function($http) {
#    routeSegmentProvider.
#        when('/', {
#            controller: 'Game',
#            resolve: {e: function() {console.log('uh');return $http.get('/api/v1/players')}},
#            resolveAs: 'aaaaaaa'
#        })
#}    ]);
app.controller 'login', [
  '$scope', 'Auth', '$http', '$location'
  ($scope, Auth, $http, $location) ->
    Auth.currentUser().then (user) ->
#      console.log user
#      console.log Auth.isAuthenticated()
      $location.path("/waiting_room")
#      $http.get('/api/v1/players').then (w) ->
#        console.log w.data.owner

    $scope.login = ->
      Auth.login($scope.user).then ->
        console.log 'ok'
        $location.path("/waiting_room");

]
app.controller 'waitingRoom', [
  '$scope', '$route', 'Auth', 'onlinePlayers'
  ($scope, $route, Auth, onlinePlayers) ->
    console.log 'online ' + onlinePlayers
    Auth.currentUser().then (user) ->
      $scope.email = user.email


      op = onlinePlayers.filter (item)->
        item != $scope.email
      $scope.players = op
#      console.log Auth.isAuthenticated()
#      console.log Auth._currentUser.email
      Appp.room = Appp.cable.subscriptions.create {channel: "RoomChannel", user_email: Auth._currentUser.email},
        connected: ->
#          console.log 'connected'
#          Appp.room.speak('fjghjhgjgjg')

        disconnected: ->
          console.log 'disconnected'
#          @perform 'unsubscribed'

        received: (data) ->
          console.log data
          if data.message == 'new user'
            $scope.players.push data.user unless data.user == $scope.email
          if data.message == 'user left'
            index = -1
            $scope.players.forEach (pl, i)->
              index = i if pl == data.user
            $scope.players.splice index, 1 if index > -1
            console.log $scope.players
          $scope.$apply()

        speak: (message)->
#          console.log 'speak'
          @perform 'speak', message: message
#    console.log $route.current.params[]
#      Appp.user = Appp.cable.subscriptions.create {channel: "UserChannel", user_email: Auth._currentUser.email},
#        connected: ->
#          console.log 'connected'
##          Appp.user.speak('get all users in the room')
#      # Called when the subscription is ready for use on the server
#
#        disconnected: ->
##          console.log 'disconnected'
#          @perform 'unsubscribed'
#
#      # Called when the subscription has been terminated by the server
#
#        received: (data) ->
#          console.log data
#      # Called when there's incoming data on the websocket for this channel
#
#        speak: (message)->
##          console.log 'speak'
#          @perform 'speak', message: message



    $scope.click = ->
#      console.log 'asdfasd'
      Appp.room.speak('fjghjhgjgjg');

    $scope.$on '$destroy', (event) ->#
#      Appp.room.disconnected()
      #    console.log Appp.room.destroy()
#      Appp.cable.subscriptions.subscriptions.splice 1, 1
  #    console.log Appp.cable.subscriptions.subscriptions
  #    unless confirm('AAAAAAAAAAAAA?')
  #    event.preventDefault()
]
app.controller 'gameCtrl', [
  '$scope'
  'owner'
  'players'
  'deck'
  'localStorageService'
  ($scope, owner, players, deck, localStorageService) ->
#    console.log players
    $scope.owner = owner
    $scope.players = players
    localStorageService.set 'deck', deck
#    console.log localStorageService.get 'deck'
#    $scope.deck = deck
#    console.log owner
#    $scope.players = angular.fromJson players.data.players
#    return
]
app.directive 'loading', [
  '$http'
  ($http) ->
    {
    restrict: 'A'
    link: (scope, elm, attrs) ->

      scope.isLoading = ->
        $http.pendingRequests.length > 0

      scope.$watch scope.isLoading, (v) ->
        if v
          $(elm).show()
        else
          $(elm).hide()
        return
      return

    }
]
app.directive 'rotate', ->
  { link: (scope, element, attrs) ->
    # watch the degrees attribute, and update the UI when it changes
    scope.$watch attrs.degrees, (rotateDegrees) ->
      #console.log(rotateDegrees);
      #transform the css to rotate based on the new rotateDegrees
      element.css
        '-moz-transform': 'rotate(' + rotateDegrees + 'rad)'
        '-webkit-transform': 'rotate(' + rotateDegrees + 'rad)'
        '-o-transform': 'rotate(' + rotateDegrees + 'rad)'
        '-ms-transform': 'rotate(' + rotateDegrees + 'rad)'
      return
    return
  }
app.directive 'draggable', ->
  (scope, element) ->
    el = element[0]
    el.draggable = true
    el.addEventListener 'dragstart', ((e) ->
      e.dataTransfer.effectAllowed = 'move'
      e.dataTransfer.setData 'Text', @id
      @classList.add 'drag'
      false
    ), false
    el.addEventListener 'dragend', ((e) ->
      @classList.remove 'drag'
      false
    ), false
    return
app.directive 'droppable', ->
  {
  scope:
    drop: '&'
    bin: '='
  link: (scope, element) ->
    el = element[0]
    el.addEventListener 'dragover', ((e) ->
      e.dataTransfer.dropEffect = 'move'
      # allows us to drop
      if e.preventDefault
        e.preventDefault()
      @classList.add 'over'
      false
    ), false
    el.addEventListener 'dragleave', ((e) ->
      @classList.remove 'over'
      false
    ), false
    el.addEventListener 'drop', ((e) ->
      # Stops some browsers from redirecting.
      if e.stopPropagation
        e.stopPropagation()
      @classList.remove 'over'
      binId = @id
      item = document.getElementById(e.dataTransfer.getData('Text'))
      # this.appendChild(item);
      # call the passed drop function
      scope.$apply (scope) ->
        fn = scope.drop()
        console.log binId
        if 'undefined' != typeof fn
          fn item.id, binId
        return
      false
    ), false
    return

  }

# ---
# generated by js2coffee 2.1.0