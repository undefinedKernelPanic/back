app.factory 'api', ($http) ->
  response = {}

  response.sendAction = ->
    $http.get 'api/v1/action', {action: 'draw_door'}

  response.getGame = ->
    $http.get('api/v1/game')


  response.getBattle = ->
    $http.get('api/v1/battle')
#      console.log resp.data.battle
#      angular.fromJson resp.data.battle
#    ), ->

  response.getOwner = ->
    $http.get('api/v1/owner').then ((resp)->
      angular.fromJson resp.data.owner
    ), ->


  response.getOtherPlayers = ->
    $http.get('api/v1/other_players').then ((resp)->
      players = []
      angular.fromJson(resp.data.players).forEach (pl)->
        players.push angular.fromJson pl
      players
    ), ->

  response.getDeck = ->
    $http.get('api/v1/deck').then ((resp)->
      resp.data.deck
    ), ->


#
#  response.owner =
#    id: 'wowow'
#    name: 'leo'
#    level: 1
#    power: 1
#    cards: [
#      {
#        id: '23'
#        type: 'race'
#        name: 'Эльф'
#        first_ability: '+1 к Смывке'
#        second_ability: 'Поднимаешься на 1 Уровень за каждого монстра которого помогаешь убить'
#      }
#      {
#        id: '23q'
#        type: 'race'
#        name: 'Хафлинг'
#        first_ability: 'что то про 1 абилку'
#        second_ability: 'чтото про 2 абилку'
#      }
#    ]
#  response.players = [
#    {
#      name: 'aa'
#      val: 4
#      id: '111'
#    }
#    {
#      name: 'tr'
#      val: 2
#      id: '1151'
#    }
#  ]
  response