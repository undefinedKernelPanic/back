function battleMonsterController($scope, $element, $attrs, storage) {
    var ctrl = this;
    //console.log(ctrl);

    $scope.card = storage.find_card(ctrl.card.id);

}


angular.module('Manchkin').component('battleMonster', {
    templateUrl: 'battleMonster.html',
    controller: battleMonsterController,
    bindings: {
        card: '<'
    //    onDelete: '&'
    }
});