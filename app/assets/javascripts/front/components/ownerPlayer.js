function OwnerPlayerController($scope, $element, $attrs) {
  var ctrl = this;

  ctrl.handleActionn = function(q) {
    console.log('???');
    ctrl.handleAction({action: q});
  };

}


angular.module('Manchkin').component('ownerPlayer', {
  templateUrl: 'ownerPlayer.html',
  controller: OwnerPlayerController,
  bindings: {
  	owner: '<',
    handleAction: '&'
  }
});