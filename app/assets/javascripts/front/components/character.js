function CharacterController($scope, $element, $attrs) {
    var ctrl = this;

    $scope.handleDrop = function(senderId, recipientId) {

      //console.log(ctrl);
        ctrl.handleAction({action: {recipientId: recipientId, senderId: senderId, type: 'dummy self'}});

    };
}


angular.module('Manchkin').component('character', {
    templateUrl: 'character.html',
    controller: CharacterController,
    bindings: {
        char: '<',
        handleAction: '&'
    }
});