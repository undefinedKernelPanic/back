function CardController($scope, $element, $attrs, storage) {
    var ctrl = this;
    //console.log(ctrl);

    $scope.card = storage.find_card(ctrl.card.id);

}


angular.module('Manchkin').component('card', {
    templateUrl: 'card.html',
    controller: CardController,
    bindings: {
        card: '<'
    //    onDelete: '&'
    }
});