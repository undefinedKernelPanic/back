OtherPlayersController = ($scope, $element, $attrs) ->
  ctrl = this

  getPlayerPlaces = (x, y, rx, ry, p) ->
    horizontAngle = 10
    availableSector = 180 - (2 * horizontAngle)
    sectorPerPlayer = availableSector / (p + 1)
    #console.log(sectorPerPlayer);
    #var e = Math.sqrt(1-(y*y/4) / (x*x/4));
    #console.log(e);
    result = []
    i = 0
    while i < p
      xd = rx * Math.cos((horizontAngle + sectorPerPlayer + i * sectorPerPlayer) * Math.PI / 180)
      yd = ry * Math.sin((horizontAngle + sectorPerPlayer + i * sectorPerPlayer) * Math.PI / 180)
      # result.push({x: Mat1
      # h.abs(2 * xd) + 'px', y: (y - yd) + 'px', color:'red'});
      xx = x / 2 + xd - (75 / 2)
      yy = y - yd - ((y - ry) / 2) - 20 - (50 / 2)
      result.push
        id: i
        x: xx + 'px'
        y: yy + 'px'
        color: 'red'
        deg: Math.atan(xd / yd * ry / rx)
      i++
    result


  $scope.handleDrop = (senderId, recipientId) ->
    ctrl.handleAction action:
      recipientId: recipientId
      senderId: senderId
      type: 'dummy player'

  y = window.screen.availHeight
  x = window.screen.availWidth
  rx = x * 0.5
  ry = y * 0.8
  p = ctrl.players.length
  #console.log(x);
  #console.log(y);
  #console.log(getPlayerPlaces(x, y, rx, ry, p));
  $scope.playersPosition = getPlayerPlaces(x, y, rx, ry, p)


angular.module('Manchkin').component 'otherPlayers',
  templateUrl: 'otherPlayers.html'
  controller: OtherPlayersController
  bindings:
    players: '<'
    handleAction: '&'
