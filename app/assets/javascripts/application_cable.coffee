#= require cable
#= require_self
#
@Appp ||= {}
Appp.cable = Cable.createConsumer('ws://127.0.0.1:34523')
