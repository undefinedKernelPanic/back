require 'json'

class ObjectConverter
  def initialize(object)
    @object = object
  end

  # def card
  #   {
  #       id: @object.id,
  #
  #   }
  # end

  def battle
    {
      owner: @object.battle_owner,
      helper: @object.battle_helper,
      monsters: @object.monsters,
      current_winner: @object.current_winner
    }.to_json
  end

  def owner
    {
        id: @object.object_id,
        name: @object.name,
        power: @object.power,
        level: @object.level,
        hand: @object.hand.map do |obj|
          { id: obj.id }
          # {
          #     id: obj.object_id,
          #     type: obj.subtype,
          #     name: obj.name,
          # }
        end
    }.to_json
  end

  def other
    {
        id: @object.object_id,
        name: @object.name,
        power: @object.power,
        level: @object.level,
        hand: @object.hand.map do |obj|
          { type: obj.card_type }
          # obj.to_json
          # {
          #     id: obj.object_id,
          #     type: obj.subtype,
          #     name: obj.name,
          # }
        end
    }.to_json
  end
end